# Liquid Labs

I've created an API where you can add orders, show all the orders for a particular company,
show the orders to a particular address, remove a specific order by providing the order id,
and show how many times a specific item has been ordered (in a descending order).

##Instructions:
To run this project you need to have installed nodeJS and install Strongloopp as following:
```bash
npm install -g strongloop
```
After Strongloop is installed, install the npm packages if needed:
```bash
npm install
```

You should have a running MongoDB instance at mongodb://localhost:27017/liquidlabs

To run the application, write:
```
node .
```

You should now be able to explore the API via the web interface and consume the five endpoints
I've implemented.

##Questions:

###Why did you pick your particular design? What assumptions did you make, and what tradeoffs did you consider?

I chose to implement the solution with Loopback because it's very easy to create APIs quickly (also because I'm more experienced with this tool). I assumed that the task required for the test was to create an API where you could consume the endpoints related to each operation described. Another assumption I made, was that the data must be stored somewhere, so I connected the app with mongoDB to persist the documents. For the last operation I decided to use the aggregation framework from mongoDB, so I could sort the result by orderedItem, and count each occurrence of every item, and finally present the results in a descending order.

###What do you like (and dislike) about Node/Javascript compared to other programming languages?

One of the things that I like the most from Javascript, is that is a language that has gained a lot of popularity
real quick, the community has grown these past years and this has boosted the creation of many innovative tools.
Furthermore, NodeJS has spread the Javascript language through all the stages of web development, joining the frontend
and backend development, with things like the MEAN stack, something never seen before. There's also a great number of
libraries that take advantage of powerful concepts like first class functions and anonymous functions to build amazing
libraries such as lodash.

Nevertheless, Javascript and NodeJS can be hard for beginners. The asynchronous aspect of NodeJS is really powerful and great
to build fast applications, but it can make the code hard to read and turn it into the so called "callback hell", unlike
languages like Python, PHP or java, where the learning curve is less steep.
