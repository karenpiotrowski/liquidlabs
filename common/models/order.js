module.exports = function(Order) {

  // Add validation
  Order.validatesUniquenessOf('orderId', {message: 'Order Id is not unique'});

  /**
   *  POST /orders
   *  @param data {object}: JSON that contains the order to be inserted
   *
   *  Remote method to create a new order.
   **/
  Order.createOrder = function (data, cb) {
    var order = {orderId: data.orderId, companyName: data.companyName, customerAddress: data.customerAddress, orderedItem: data.orderedItem};
    Order.create(order, function(err, instance) {
      if (err) {
        console.log(err);
        cb(err, null);
        return;
      }
      cb(null, instance);
    });
  }

  Order.remoteMethod(
    'createOrder',
    {
      accepts: {arg: 'data', type: 'object', http: {source: 'body'}},
      returns: {arg: 'order', type: 'object'},
      http: {path: '/', verb: 'post'},
      description: ['Create an order']
    }
  )

  /**
   *  GET /Orders/findByCompanyName
   *  @param name {string}: Name of the company
   *
   *  Remote method to find the orders from a particular company
   **/
   Order.findByCompanyName = function (name, cb) {
     Order.find({where: {companyName: name}}, function(err, order) {
       if (err) {
         console.log(err);
         cb(err, null);
         return;
       }
       cb(null, order);
     })
   }

  Order.remoteMethod(
    'findByCompanyName',
    {
      accepts: {arg: 'name', type: 'string', http: {source: 'query'}},
      returns: {arg: 'orders', type: 'Array'},
      http: {path: '/findByCompanyName', verb: 'get'},
      description: ['Find all the orders by a given company name']
    }
  )

  /**
   *  GET /Orders/findByCustomerAddress
   *  @param address {string}: The address of the customer
   *
   *  Remote method to find the orders by providing the address of the customer
   **/
   Order.findByCustomerAddress = function (address, cb) {
     Order.find({where: {customerAddress: address}}, function(err, order) {
       if (err) {
         console.log(err);
         cb(err, null);
         return;
       }
       cb(null, order);
     })
   }

  Order.remoteMethod(
    'findByCustomerAddress',
    {
      accepts: {arg: 'address', type: 'string', http: {source: 'query'}},
      returns: {arg: 'orders', type: 'Array'},
      http: {path: '/findByCustomerAddress', verb: 'get'},
      description: ['Find all the orders by a given address']
    }
  )

  /**
   *  DEL /orders/{id}
   *  @param id {string}: The order id
   *
   *  Remote method to remove an order by providing the id of the order (orderId)
   **/
  Order.deleteOrder = function(id, cb) {
    Order.remove({orderId: id}, function(err, deleted){
      if (err) {
        console.log(err);
        cb(err, null);
        return;
      }
      cb(null, deleted);
    });
  }

  Order.remoteMethod(
    'deleteOrder',
    {
      accepts: {arg: 'id', type: 'string', http: {source: 'path'}},
      http: {path: '/deleteOrder/:id', verb: 'del'},
      description: ['Delete an order']
    }
  )

  /**
   *  GET /Orders/getTimesOrdered
   *
   *  Remote method to retrieve the times every item has been ordered, in a descending
   *  order.
   **/
   Order.getTimesOrdered = function(cb) {
     var orderCollection = Order.getDataSource().connector.collection(Order.modelName);
     orderCollection.aggregate(
       [
         {$group: {_id: {orderedItem: "$orderedItem"}, timesOrdered: {$sum: 1}}},
         {$sort:  {count: -1}}
       ],
      function(err, result) {
       if (err) {
         cb(err, null);
       } else {
         cb(null, result);
       }
     });
   }

  Order.remoteMethod(
    'getTimesOrdered',
    {
      returns: {arg: 'orders', type: 'Array'},
      http: {path: '/getTimesOrdered', verb: 'get'},
      description: ['Find all the orders by a given company name']
    }
  )


};
